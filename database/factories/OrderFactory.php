<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Order;
use Faker\Generator as Faker;

$factory->define(Order::class, function (Faker $faker) {
    
        $faker = \Faker\Factory::create('es_ES'); 
    return [
        'order_id' => $faker->unique()->numberBetween($min = 1000, $max = 9000),
        'company' => $faker->company,
        'ship_date' => $faker->date($format = 'Y-m-d', $max = now()),
        'country' => "España",
        'status' => $faker->randomElement([Order::SUCCESS, Order::PENDING, Order::CANCELLED]),
        'type' => $faker->randomElement([Order::RETAIL, Order::ONLINE, Order::DIRECT])
    ];

});
