<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Order;
class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string('order_id');
            $table->string('country');
            $table->date('ship_date');
            $table->string('company');
            $table->enum('status', [Order::SUCCESS, Order::PENDING, Order::CANCELLED])->default(Order::PENDING);
            $table->enum('type', [Order::RETAIL, Order::ONLINE, Order::DIRECT])->default(Order::DIRECT);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
