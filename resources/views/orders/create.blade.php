@extends("layouts.app")

@section('title', __("Crear pedido") )

@section("content")
	@include("orders.form")
@endsection