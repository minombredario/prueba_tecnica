@push('css')
    <style>
        .panel{
            background-color: #ffffff;
            border-radius: 6px;
            border-bottom: 2px solid blue;
            width: 100%;
            overflow: auto;
        }
        
        .header{
            color: #19213d;
            font-size 1.1em;
            padding: 10px;
        }

        .header h2 .title{
           display: inline-block;
        }

        .panel .inside{
            padding: 16px;
			height: 80%;
        }
    </style>
@endpush

<div class="container-fluid">
    
    @include('partials.form_errors')
    <div class="panel shadow">
        <div class="header position-relative">
            <h2 class="title">
                <i class="fas fa-plus"></i> {{ $title }}
            </h2>
            <a href="{{ route('orders') }}" class="btn btn-sm btn-info position-absolute top-0 end-0">
                {{ _('Volver') }}
            </a>
        </div>
         
        <div class="inside">
            {!! Form::model($order, $options) !!}
    
            @isset($update)
                @method("PUT")
            @endisset

            @csrf
            
            <div class="row">
                <div class="col-md-9 col-lg-10 order-md-1 order-lg-0 mb-4">
                    <div class="row">   
                        
                        <div class="col-md-6">
                            {!! Form::label('company', __("Empresa")) !!}
                            <div class="input-group" >
                                {!! Form::label('company', '<i class="far fa-keyboard"></i>', [ 'class' => 'input-group-text'], false) !!}
                                {!! Form::text('company', null, ['class' => 'form-control' .($errors->has('company') ? ' is-invalid' : ''), 'required' ]) !!}
                            </div>

                            @if ($errors->has('company'))
                                <div class="{{$errors->has('company') ? 'invalid' : 'valid'}}-feedback" style="{{$errors->has('company') ? 'display:block' : ''}}">
                                    <strong>{{ $errors->first('company') }}</strong>
                                </div>
                            @endif
                            
                        </div>

                        <div class="col-md-6">
                            {!! Form::label('ship_date', __("Fecha")) !!}
                            <div class="input-group" >
                               {!! Form::date('ship_date', isset($update) ? null : now() , ['class' => 'form-control-plaintext' .($errors->has('ship_date') ? ' is-invalid' : ''), 'required','readonly']) !!}
                            </div>

                            @if ($errors->has('ship_date'))
                                <div class="{{$errors->has('ship_date') ? 'invalid' : 'valid'}}-feedback" style="{{$errors->has('ship_date') ? 'display:block' : ''}}">
                                    <strong>{{ $errors->first('ship_date') }}</strong>
                                </div>
                            @endif
                            
                        </div>
                    </div> 

                    <div class="row mt-3">
                        <div class="col-md-4">
                            {!! Form::label('country', __("País")) !!}
                            <div class="input-group mb-3">
                                {!! Form::label('country', '<i class="far fa-keyboard"></i>', [ 'class' => 'input-group-text' ], false) !!}
                                {!! Form::select('country', ['España' => 'España', 'Francia' => 'Francia', 'Portugal' => 'Portugal'], null, ["class" => "form-select", 'placeholder' => __('Selecciona un país'), 'required']) !!}
                            </div>    
                        </div>  
                        <div class="col-md-4">
                            {!! Form::label('status', __("Status")) !!}
                            <div class="input-group mb-3">
                                {!! Form::label('status', '<i class="far fa-keyboard"></i>', [ 'class' => 'input-group-text' ], false) !!}
                                {!! Form::select('status', ['Success' => 'Success','Pending' => 'Pending','Cancelled' => 'Cancelled'], null, ["class" => "form-select", 'placeholder' => __('Selecciona un status'), 'required']) !!}
                            </div>    
                        </div>
                        <div class="col-md-4">
                            {!! Form::label('type', __("Type")) !!}
                            <div class="input-group mb-3">
                                {!! Form::label('type', '<i class="far fa-keyboard"></i>', [ 'class' => 'input-group-text' ], false) !!}
                                {!! Form::select('type', ['Retail' => 'Retail', 'Direct' => 'Direct','OnLine' => 'OnLine'], null, ["class" => "form-select", 'placeholder' => __('Selecciona un type'), 'required']) !!}
                            </div>    
                        </div>
                        
                        
                    </div>
                </div> 
               

               
            </div>   
            
            
                

           
           
            <div class="row mt-3">
                <div class="col">
                    {!! Form::submit($textButton, ['class' => 'btn btn-primary mt-2']); !!}
                </div>
            </div>
            

                
            {!! Form::close() !!}
        </div>
    </div>
</div>

@push('js')
    
    <script>
        $(document).ready(function() {
            
        });

        

    </script>
@endpush

