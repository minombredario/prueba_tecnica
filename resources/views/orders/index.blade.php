@extends("layouts.app")

@push('css')    
    <style>
        .page{
            padding-top: 16px;
            padding-bottom: 16px;
            overflow: auto;
        }

        .panel{
            background-color: #ffffff;
            border-radius: 6px;
            border-bottom: 2px solid blue;
            width: 100%;
            overflow: auto;
        }
        
        .header{
            color: #19213d;
            font-size 1.1em;
            text-align: center;
            padding: 5px;
        }
        .header h2.title{
           display: inline-block;
        }

        .header .form-switch{
            padding: 12px 16px;
        }
		
        .header ul{
            display: inline-block;
            float: right;
            margin-right: 12px;
            z-index: 9;
        }

        .header ul li{
            display: inline-block;
			position: relative;
        }

        .header ul li a{
            color: #333333;
            display: inline-block;
            padding: 10px;
            text-decoration: none;
        }

        .header ul li a:hover{
            background-color: #f6f6f6;
			color: #222222;
        }

        .header ul li ul{
            background-color: #ffffff;
            border: 1px solid #f0f0f0;
            display: none;
            float: none;
            position: absolute;
            right: -16px;
            width: 200px;
        }

        .header ul li ul{

        }

        .header ul li ul li{
            display: inline-block;
            border-bottom: 1px solid #f0f0f0;
            width: 100%;
        }

        .header ul li ul li a{
            display: block;
        }

        .header ul li ul li:last-child{
            border-bottom: 0px;
        }
		
		.panel .inside{
            padding: 16px;
			height: 80%;
        }
		
        .panel .inside .nav .nav-link{
            border-bottom: 1px solid blue;
            border-right: 1px solid blue;
            margin-right: 8px;
        }

		.panel .inside .nav .form_search{
            display: none;
			margin-bottom: 16px;
        }
        
        .opts{
            text-align: right;
        }
	
        .opts a{
            border-radius: 4px;
            border: 1px solid #dedede;
            display: inline-block;
            padding: 8px 12px;
        }
				
					
			

        
    </style>
@endpush
@section('title', __('Pedidos') )

@section("content")
    @include("orders.list")
@endsection


@push('js')
    <script>
        jQuery(document).ready(function () {
            $('.btn-search').click(function (e) {
                e.preventDefault();
                $('.form-search').toggleClass('d-none');
            });

            
        });

    </script>
@endpush