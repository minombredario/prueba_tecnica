@extends("layouts.app")

@section('title', __("Editar pedido") )

@section("content")
	@include("orders.form")
@endsection