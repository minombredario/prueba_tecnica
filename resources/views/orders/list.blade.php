
<!-- order section -->
<div class="container-fluid">
    <div class="panel panel-table shadow">
        <div class="header">
            <h2 class="title">
                {{ __('Pedidos') }}
			</h2>
			<ul>
				
				<li>
					<a href="{{ route('orders.create') }}">
						{{ __('Crear pedido') }}
					</a>
				</li>
				
				<li>
					
                    <a href = "javascript:{}" class = "nav-item nav-link" id="all">
                        <i class="fas fa-globe-americas"></i> {{ __('Todos') }}
                    </a> 
                    
						
				</li>
				<li>
					<a href="#" class="btn-search">
						<i class="fas fa-search"></i> {{ __('Buscar') }}
					</a>
				</li>
			</ul>
		</div>
		<div class="inside">
			<div class="form-search d-none my-4">
				
                       
				
					<div class="row">
						
						<div class="col-md-6">
						    {!! Form::text('search', session('search[orders]'), ['class' => 'form-control', 'placeholder' => __('Busqueda') ]) !!}
						</div>
						<div class="col-md-4">
							{!! Form::select('type',  ['1' => __('Número del pedido'), '2' => __('Empresa'), '3' => __('Status'), '4' => __('Tipo') ] , session('type[orders]'), ['class' => 'form-select']) !!}
						</div>
						<div class="col-md-2">
							<button id="submit" class="btn btn-dark w-100"> {{ __('Buscar') }}</button>
						</div>
					</div>

            	
				
			</div>
			<div class="content">
                @include('orders.table')
			</div>
		</div>
	</div>
    
</div>
<!-- order section end -->
@push("js")
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script>
        
            var page = 1;
            var search = '';
            var type = '';
            var url = '';
            $(document).on('click','.pagination a', function(e){
                e.preventDefault();
                page = $(this).attr('href').split('page=')[1];
                
                fetch_data(page, search, type);
            });

            $(document).on('click', '.delete-record', function () {

                const btn = $(this);
                route = btn.data("route");
                
            }); 

            $(document).ready(function () {
                $('#submit').click(function (e) {
                    e.preventDefault();
                    type = $("select[name='type']").val();
                    search = $("input[name='search']").val();
                    fetch_data(page, search, type)
                });

                $('#all').click(function (e) {
                    e.preventDefault();
                    type = '';
                    search = '';
                    fetch_data(page, search, type, 'all')
                });

                
            });

            function fetch_data(page, search, type, filter = ''){
                
                var options = filter == 'all' ? "&filter=all" : "&search=" + search + "&type=" + type;
               
                $.ajax({
                    url: "{{env('URL_SERVER')}}/orders?page=" + page + options,
                }).done(function(data){
                    $('.content').html(data);
                })
			}

            function deleteOrderId(){
                var token = $("meta[name='csrf-token']").attr("content");
                const url = route;
            
                $.ajax({
                    method: "GET",
                    url: url,
                    success: function (data) {
                        window.location.reload();
                    },
                    error: function (error) {
                        console.log(error)
                    }
                })
                
                
            }
        
    </script>
@endpush