
    <table class="table table-striped mtop16">
        <thead>
            <tr>
                <td class="align-middle">{{ __('# Pedido') }}</td>
                <td class="align-middle">{{ __('Empresa') }}</td>
                <td class="align-middle">{{ __('País') }}</td>
                <td class="align-middle">{{ __('Fecha') }}</td>
                <td class="align-middle">{{ __('Status') }}</td>
                <td class="align-middle">{{ __('Tipo') }}</td>
                <td></td>
            </tr>
        </thead>
        <tbody>
            <!-- order -->
            @forelse ($orders as $order)
            <tr>
                
                <td class="align-middle">{{ $order->order_id }}</td>
                <td class="align-middle">{{ $order->company }}</td>
                <td class="align-middle">{{ $order->country }}</td>
                <td class="align-middle">{{ \Carbon\Carbon::parse( $order->ship_date )->format('d-m-Y')}}</td>
                <td class="align-middle">
                    @php
                        $type = '';
                    @endphp
                    @switch($order->status)
                        @case('Success')
                            @php $type = 'success' @endphp
                            @break
                        @case('Pending')
                            @php $type = 'warning' @endphp
                            @break
                        @case('Cancelled')
                            @php $type = 'danger' @endphp
                            @break                            
                    @endswitch
                    <span class="badge bg-{{$type}} text-dark" style="padding:5px">{{ $order->status }}</span>
                </td>
                <td class="align-middle">
                    @php
                        $type = '';
                    @endphp
                    @switch($order->type)
                        @case('Retail')
                            @php $type = 'success' @endphp
                            @break
                        @case('Direct')
                            @php $type = 'primary' @endphp
                            @break
                        @case('OnLine')
                            @php $type = 'danger' @endphp
                            @break                            
                    @endswitch
                    <span class="text-{{$type}}">{{ $order->type }}</span>
                    
                </td>
                <td>
                    <div class="opts">
                        
                             
                        <a class="btn btn-danger delete-record" href="#" data-route="{{ route('orders.destroy',  ["order" => $order])}}" title="{{ __("Eliminar") }}" data-id="{{ $order->id }}" data-bs-toggle="modal" data-bs-target="#deleteModal">
                            <i class="fas fa-trash"></i>
                        </a>
                           
                        <a class="btn btn-info" href="{{ route('orders.edit',$order)}}" data-bs-toggle="tooltip" data-bs-placement="top" title="{{ __("Editar") }}">
                            <i class="fas fa-edit"></i>
                        </a>
                            
                           
                        
                    
                        
                    </div>
                </td>  
            </tr> 
            @empty
            <tr>
                <div class="empty-results">
                    {!! __("Ningún resultado") !!}
                </div>
            </tr>
            @endforelse
        </tbody>
    </table>
    @include('modals.delete')
    <div class="d-flex justify-content-center">
        {!! $orders->links() !!} 
    </div>
    <span class="float-end"> {{$orders->firstItem() . "-" . $orders->lastItem()}} {{ __('de') }} {{ $orders->total()}} </span>
