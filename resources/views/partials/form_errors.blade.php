@push('css')
    <style>
        .form-errors {
            background-color: red;
            padding-left: 10px;
        }

        .form-errors p {
            color: #fff !important;
        }
    </style>
@endpush
@if ($errors->any())
    <div class="form-errors">
        @foreach ($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
@endif
