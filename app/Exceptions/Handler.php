<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $exception)
    {
        if ($exception instanceof CustomException) {
            
        }
        
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $e)
    {  
        if($this->isHttpException($e)){
          
        switch (intval($e->getStatusCode())) {
            // not found
            case 404:
                session()->flash("message", ["warning", __('Página no encontrada')]);
                    return redirect("home");                
                break;
            case 419:
                
                session()->flash("message", ["warning", __("Tu sesión ha expirado, por favor logueate de nuevo")]);
                return redirect("home");
                break;
            // internal error
            case 500:
                session()->flash("message", ["warning", __('Error del servidor')]);
                return redirect("home");
              

            default:
                return $this->renderHttpException($e);
                break;
        }
    }else{
        if ($e instanceof \Illuminate\Session\TokenMismatchException) {
           return redirect('/');
        }
        if ($e instanceof ModelNotFoundException) {
            $e = new NotFoundHttpException($e->getMessage(), $e);
        }

        return parent::render($request, $e);
    }
    }
}
