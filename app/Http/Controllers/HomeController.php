<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\ManageOrders;

class HomeController extends Controller
{
    use ManageOrders;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function resetSessions(){
        session()->remove('filter[orders]');
        session()->remove('search[orders]');
        session()->remove('type[orders]');
    }
}
