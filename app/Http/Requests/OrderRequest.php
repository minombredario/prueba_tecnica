<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\Models\Order;
class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case "POST": {
                return [
                    'company' => 'required',
                    'status' => Rule::in(Order::SUCCESS, Order::PENDING, Order::CANCELLED),
                    'type' => Rule::in(Order::RETAIL, Order::DIRECT, Order::ONLINE),
                    'ship_date' => 'required|date|date_format:Y-m-d|date_equals:' . date('Y-m-d'),
                    
				];
            }   
            case "PUT": {
                return [
                    'company' => 'required',
                    'status' => Rule::in(Order::SUCCESS, Order::PENDING, Order::CANCELLED),
                    'type' => Rule::in(Order::RETAIL, Order::DIRECT, Order::ONLINE),
                    'ship_date' => 'date'
                ];
            }
            default: {
                return [];
            }
        }
    }

}

