<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Order extends Model
{
    const SUCCESS = 'Success';
    const PENDING = 'Pending';
    const CANCELLED = 'Cancelled';


    const RETAIL = 'Retail';
    const DIRECT = 'Direct';
    const ONLINE = 'OnLine';

    protected $fillable = [
        'order_id','country','ship_date', 'company', 'status', 'type'
    ];

    public function scopeFiltered(Builder $builder) {
        
        
        session()->remove('delete[orders]');

        if(session()->has('filter[orders]')){
            if(session('filter[orders]') === 'all')  {
                session()->remove('filter[orders]');
                session()->remove('search[orders]');
                session()->remove('type[orders]');
            }
        };
       
        if(session()->has('search[orders]')){
            switch (session('type[orders]')) {
                case '1':
                    $field = 'order_id';
                    break;
                case '2':
                    $field = 'company';
                    break;
                case '3':
                    $field = 'status';
                    break;
                case '4':
                    $field = 'type';
                    break;
                default:
                    break;
            }

            $builder->where($field, 'LIKE', '%' . session('search[orders]') . '%');
        }
         
       return $builder->orderBy('order_id','ASC')->paginate(9);
    }
}
