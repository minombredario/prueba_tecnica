<?php
namespace App\Traits;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Order;
use App\Http\Requests\OrderRequest;
use Auth, DB, Hash;

trait ManageOrders {
    public function orders(){
        

         if(!request('page')){
            $this->resetSessions();
        };
        $view = 'orders.index';

         if (request('search')) {
            session()->put('search[orders]', request('search'));
            session()->save(); 
        }

        if (request('type')) {
            session()->put('type[orders]', request('type'));
            session()->save();
        }
        
        if (request('filter')) {
            session()->put('filter[orders]', request('filter'));
            session()->save();
        }

        if(request()->ajax()){
           $view = 'orders.table'; 
        }

        $orders = Order::Filtered();    
        
        return view($view, compact('orders'));
    }

    public function createOrder() {
        
        $order = new Order;
        $title = __("Crear pedido");
        $textButton = __("Guardar");
        $options = ['route' => ['orders.store']];
        return view('orders.create', compact('title', 'order', 'options', 'textButton'));

    }

    public function storeOrder(OrderRequest $request, Order $order) {
       
       
        try {
            DB::beginTransaction();
            $last = DB::SELECT("SELECT `order_id` from `orders` ORDER BY `order_id` DESC LIMIT 1");
           
            $order = Order::create($this->orderInput($last[0]->order_id + 1));
           
           DB::commit();
            session()->flash("message", ["success", __("Pedido creado satisfactoriamente")]);
            return redirect(route('orders.edit', ['order' => $order]));
        } catch (\Throwable $exception) {
            session()->flash("message", ["danger", $exception->getMessage()]);
            return back()->withInput();
        }
    }

    public function editOrder(Order $order) {
        
        $title = __("Editar #:order", ["order" => strtolower($order->order_id)]);
        $textButton = __("Actualizar pedido");
        $options = ['route' => ['orders.update', ["order" => $order]]];
        $update = true;
        return view('orders.edit', compact('title', 'order', 'options', 'textButton', 'update'));
    }

    public function updateOrder(OrderRequest $request, Order $order) {
        
        try {
            DB::beginTransaction();
                $order->fill($this->orderInput($order->order_id))->save();
            DB::commit();

            session()->flash("message", ["success", __("Pedido actualizado satisfactoriamente")]);
            return back();
        } catch (\Throwable $exception) {
            DB::rollBack();
            session()->flash("message", ["danger", $message]);
            return back()->withInput();
        }
    }

    public function destroyOrder(Order $order){   

       try {
            if (request()->ajax()) {
                
                $order->delete();
                session()->flash("message", ["success", __("El pedido :order ha sido eliminado correctamente", ["order" => $order->order_id])]);
            } else {
                abort(401);
            }
        } catch (\Exception $exception) {
            session()->flash("message", ["danger", $exception->getMessage()]);
        }
    }

    protected function orderInput($order_id): array {
        return [
            'order_id' => $order_id,
            'company' => e(request('company')),
            'ship_date' => request('ship_date'),
            'country' => request('country'),
            'status' => request('status'),
            'type' => request('type'),
        ];
    }

}
