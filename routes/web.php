<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');

Route::group(['middleware' => ['web', 'auth']], function() {
    Route::get('/orders', 'HomeController@orders')->name('orders');
    Route::post('/orders', 'HomeController@orders')->name('orders');

    Route::get('/orders/create', 'HomeController@createOrder')->name('orders.create');
    Route::post('/orders/store', 'HomeController@storeOrder')->name('orders.store');

    Route::get('/orders/{order}', 'HomeController@editOrder')->name('orders.edit');
    Route::put('/orders/{order}', 'HomeController@updateOrder')->name('orders.update');

    Route::get('/orders/delete/{order}', 'HomeController@destroyOrder')->name('orders.destroy');
    
});

Route::get('/home', 'HomeController@orders')->name('home');
